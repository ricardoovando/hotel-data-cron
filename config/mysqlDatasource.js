/**
 * MysqlDatasource.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */
//var mysql = require('mysql');
var DataSource = require('loopback-datasource-juggler').DataSource;

var connections = {
	cocha:new DataSource(Koa.config.mysqlConf.cocha)
};

connections.cocha.on('error',function(e){
	console.log('connection error ',JSON.stringify(e));
})

module.exports = {
	query: (query,dbName) => {
		return new Promise((resolve, reject) => {
			connections[dbName].connector.execute(query, [], function(error,results){
			    if (error) {
			    	console.log("DB",dbName,JSON.stringify(error));
					connections.cocha = new DataSource(Koa.config.mysqlConf.cocha);
					connections[dbName].connector.execute(query, [], function(error2,results2){
						if(error2){
							reject(error2);
						} else {
							resolve(results2);
						}
					})
			    } else {
			    	resolve(results);
			    }				
			});
	  	});
	}
};