'use strict';
/* jshint strict: false, esversion: 6 */

module.exports = {
	fillDynamicPackageData: {
		time: '0 20 * * 1',
		service: 'HotelService',
		action: 'fillDynamicPackageData'
	},
	fillAmenityData: {
		time: '0 12 * * 3',
		service: 'AmenityService',
		action: 'feedAmenityData'
	} 
};