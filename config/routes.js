'use strict';
/* jshint strict: false, esversion: 6 */

module.exports = {
	'/hotels/feedDynamicPackage' : {
		method: 'GET' ,
		controller: 'HotelController',
		action: 'feedDynamicPackage'
	},
	'/hotels/feedRegion/:id' : {
		method: 'GET' ,
		controller: 'HotelController',
		action: 'feedDynamicPackage'
	},
	'/hotels/feedAmenityData' : {
		method: 'GET' ,
		controller: 'HotelController',
		action: 'feedAmenityData'
	}	
};
