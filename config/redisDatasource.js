'use strict';
/* jshint strict: false, esversion: 6 */

let redisLib = require("redis");


module.exports = (_config, _prefix) => {
	let redis = redisLib.createClient(_config);
	_prefix = (_prefix)? (_prefix + ':') : '';

	redis.on("error", function(err) {
		console.log("redis error",err);
		Koa.log.error('[ERROR][RedisModel][onError] - ' + err);
	});

	function setToRedis(key, value, expire, callback) {
		let expTime = expire || _config.expire;
		if(callback){
			redis.set(key, JSON.stringify(value), function(err, reply) {
				if (err) {
					callback({
						CODE: 'RD-500',
						KEY: key,
						DETAIL: err
					}, null);
				} else {
					if(expTime && (!expire || expire!=-1)){
						redis.expire(key, expTime);
					}
					callback(null, {KEY: key});
				}
			});
		} else {
			redis.set(key, JSON.stringify(value));
			if(expTime && (!expire || expire!=-1)){
				redis.expire(key, expTime);
			}
		}

	}

	function getFromRedis(key,callback) {
			redis.get(_prefix + key, function(err, reply) {
				if (err) {
					callback({
						status: 500,
						message: {
							code: 'RD-500',
							msg: 'The key ' + key + ' could not be recovered'
						},
						data: err
					}, null);
				} else
				if (reply === null) {
					callback ({
						status: 404,
						message: {
							code: 'RD-404',
							msg: 'The key ' + key + ' has no associated value'
						},
						data: {}
					}, null);
				} else
				if (reply) {
					callback(null,JSON.parse(reply));
				}
			});
	}

	function delFromRedis(key,callback) {
			redis.del(_prefix + key, function(err, reply) {
				if (err) {
					callback({
						status: 500,
						message: {
							code: 'RD-500',
							msg: 'The key ' + key + ' could not be deleted'
						},
						data: err
					}, null);
				} else {
					callback(null,(reply > 0));
				}
			});
	}

	function existInRedis(key,callback) {
			redis.exists(_prefix + key, function(err, reply) {
				if (err) {
					callback({
						status: 500,
						message: {
							code: 'RD-500',
							msg: 'The key' + key + ' could not be validated'
						},
						data: err
					}, null);
				} else {
					callback(null,(reply > 0));
				}
			});
	}

	return {
		set: setToRedis,
		get: getFromRedis,
		del: delFromRedis,
		exist: existInRedis
	}
};
