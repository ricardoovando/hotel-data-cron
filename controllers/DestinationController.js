'use strict';
/* jshint strict: false, esversion: 6 */

const destinationModel = require('../models/mysql/Destination');

async function getDestinations(ctx) {
	let results = await destinationModel.getDestinations();
	let parsed = [];
	for(let i=0;i<results.length;i++) {
		let region = await destinationModel.getDestination(_.trim(results[i].ean));
		parsed.push({name:results[i].name,ean:results[i].ean,countryCode:(region[0] ? region[0].countryCode : 'XX')});
	}
	ctx.body = parsed;
}

async function getDestinationHotels(ctx) {
	let suppliersMapping = ctx.params.suppliers || Koa.config.default.mappings;
	suppliersMapping = (typeof suppliersMapping === 'string') ? suppliersMapping.split(',') : suppliersMapping;
	let minStars = ctx.params.stars || Koa.config.default.stars;
	ctx.body = await destinationModel.getDestinationHotels(ctx.params.id,minStars,suppliersMapping);
}

module.exports = {
	getDestinations:getDestinations,
	getDestinationHotels:getDestinationHotels
};