'use strict';
/* jshint strict: false, esversion: 6 */

const hotelService = require('../services/HotelService');
const amenityService = require('../services/AmenityService');


module.exports = {
	 feedDynamicPackage:feedDynamicPackage
	,feedAmenityData:feedAmenityData
}

async function feedDynamicPackage(ctx){
	hotelService.fillDynamicPackageData(ctx.params);
	ctx.body = {
		msg:"cron dynamic package feed running"
	};
}

async function feedAmenityData(ctx){
	amenityService.feedAmenityData(ctx.params);
	ctx.body = {
		msg:"cron dynamic package amenity feed running"
	};
}