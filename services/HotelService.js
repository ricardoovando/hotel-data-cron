'use strict';
/* jshint strict: false, esversion: 6 */

var ws  		      = require('cocha-external-services').webServices;
var slackService  = require('./SlackService');
var util          = require('./UtilService');
var async         = require('async');

async function fillDynamicPackageData(_params){
    _params = _params || {};
  try{
    // Hotels Async
    let destinations = await getRegions(_params.id);
    let errors = [];
    let errorHotels = {};
    slackService.log('info', 'inicio: ' + destinations.length + ' regiones, ' + moment().format(), 'Cron Dynamic Package');
    for(let i=0;i<destinations.length;i++){
        let hotels = await getHotels(_.trim(destinations[i].ean));
        let info = {};
        if(!hotels) {
          errors.push({region:destinations[i].ean,msg:'no hotels'});
          console.log('no hotels',destinations[i].ean);
          Koa.staticData.set('region:'+_.trim(destinations[i].ean),{});
          Koa.supplierData.set('region:'+_.trim(destinations[i].ean),{hotels:[],static:[]});
          continue;
        }
        let hotelsArray  = _.values(hotels.ean);
        let hotelsSearch = _.keys(hotels.ean);        
        await new Promise((resolve,reject) => {

        async.mapLimit(hotelsArray, 5, async function(hotel) {
            let response = await getHotelData(hotel);
            
            if(!response) {
              if(!errorHotels[destinations[i].ean]){
                errorHotels[destinations[i].ean] = {};
              }
              errorHotels[destinations[i].ean][hotel] = true;
              console.log('no hotel data',hotel);
              errors.push({region:destinations[i].ean,hotel:hotel,msg:'no hotel data'});
              return null;
            }
            //save to hotel database
            Koa.staticData.set('hotel:'+response.info.id,response);
            delete response.info.roomFeesDescription;          
            delete response.info.checkInInstructions;          
            delete response.info.specialCheckInInstructions;
            delete response.info.PropertyAmenity;          
            info['hotel:'+response.info.id] = response.info;
            info['hotel:'+response.info.id].categories = response.categories;
            info['hotel:'+response.info.id].image = response.image;
            info['hotel:'+response.info.id].amenities = response.amenities;
            await util.sleep(100);
            return true;
        }, function (err, results) {
            console.log("region",i,destinations[i].ean);
            if (err){
              console.log("ERROR:","region:",destinations[i].ean,"hoteles:",JSON.stringify(hotelsArray),JSON.stringify(err),JSON.stringify(results));
            } 
            if(errorHotels[destinations[i].ean]){
              console.log("missing hotels data",JSON.stringify(errorHotels[destinations[i].ean]));
            }
            let region = {
                 hotels:hotelsSearch
                ,mapping:hotels
                ,info:info
                ,countryCode:destinations[i].countryCode
            };
            //save to region database composer
            Koa.staticData.set('region:'+_.trim(destinations[i].ean),region);
            //save to region database supplier
            Koa.supplierData.set('region:'+_.trim(destinations[i].ean),{hotels:hotelsSearch,static:[]});
            resolve(true);            
        })

        });
    }
    slackService.log('info', 'fin: ' + destinations.length + ' regiones, ' + moment().format(), 'Cron Dynamic Package');        
    slackService.log('info', 'error: '+JSON.stringify(errors) , 'Cron Dynamic Package');
  }catch(err){
      console.log("errores",JSON.stringify(err));
      slackService.log("info","Error:"+JSON.stringify(err), 'Cron Dynamic Package');
  }
}


async function getHotelData(_hotel){
  let _getHotelUrl = Koa.config.path.getHotel + _hotel;  
  return new Promise((resolve, reject) => {
      ws.get('getHotel', _getHotelUrl , null, null, function(err, result){
        if(err){
          resolve(null);
        } else {
          resolve(result);
        }
      });
  });  
}

async function getRegions(_region){
  let _getRegionsUrl = (_region) ? Koa.config.path.getRegions+'/'+_region : Koa.config.path.getRegions;
  return new Promise((resolve, reject) => {
      ws.get('getRegions', _getRegionsUrl , null, null, function(err, result){
        if(err){
          resolve(null);
        } else {
          resolve(result);
        }
      });
  });
}


async function getHotels(_region){
  let _getHotelsUrl = Koa.config.path.getRegionHotels + _region;  
  return new Promise((resolve, reject) => {
      ws.get('getHotelsFromRegion', _getHotelsUrl , null, null, function(err, result){
        if(err){
          resolve(null);
        } else {
          resolve(result);
        }
      });
  });
}


module.exports = {
  fillDynamicPackageData:fillDynamicPackageData
}