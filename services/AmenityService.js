'use strict';
/* jshint strict: false, esversion: 6 */

var slackService  = require('./SlackService');
var util          = require('./UtilService');
var async         = require('async');
var http          = require('http');
var fs            = require('fs');
var unzip         = require('unzip');
var ws            = require('cocha-external-services').webServices;
var amenityModel  = require('../models/mysql/Amenity');
var hotelModel    = require('../models/mysql/Hotel');


async function feedAmenityData(){
    try {
      slackService.log('info', 'inicio amenity data ' + moment().format(), 'Cron Dynamic Package'); 
      let linkAmenities =  Koa.config.files.sourceAmenities;
      await download(linkAmenities,Koa.config.files.destAmenitiesZip);
      await unzipFile(Koa.config.files.destAmenitiesZip,Koa.config.files.destAmenitiesDir);

      let linkHotels =  Koa.config.files.sourceHotels;
      await download(linkHotels,Koa.config.files.destHotelsZip);
      await unzipFile(Koa.config.files.destHotelsZip,Koa.config.files.destHotelsDir);

      await amenityModel.clear();
      let amenities = await getAmenityTypes();
      await insertAmenities(Koa.config.files.destAmenitiesFile,amenities);
      await insertHotels(Koa.config.files.destHotelsFile);
    
      let regions = await getRegions();
      for(let i=0;i<regions.length;i++){
        let hotels = await getHotels(regions[i].ean);
        if(!hotels.ean){
          console.log("region with no hotels",regions[i].ean);
          continue;
        } else {
          let hotelsSearch = _.keys(hotels.ean);
          for(let j=0;j<hotelsSearch.length;j++){
            let hotelAmenities = await amenityModel.getHotelAmenities(hotelsSearch[j]);
            let isAllInclusive = await hotelModel.isAllInclusive(hotelsSearch[j]);
            let parsedAmenities = parseAmenities(hotelAmenities,amenities,isAllInclusive);
            //console.log("parsed amenities",parsedAmenities);
            if(parsedAmenities && Object.keys(parsedAmenities).length > 0){
              amenityModel.del(hotelsSearch[j]);
              for (var amenityName in parsedAmenities) {
                if(_.includes(parsedAmenities[amenityName].consistency,true) && _.includes(parsedAmenities[amenityName].consistency,false)){
                  console.log('incongruencia',hotelsSearch[j],parsedAmenities[amenityName]);
                } else {
                  try{
                    await amenityModel.insert(hotelsSearch[j],parsedAmenities[amenityName].attribute,'es_ES','');
                  }catch(err){
                    console.log("insert fail",JSON.stringify(err),parsedAmenities[amenityName].attribute,hotelsSearch[j]);
                    await util.sleep(100);
                    await amenityModel.insert(hotelsSearch[j],parsedAmenities[amenityName].attribute,'es_ES','');
                  }
                }
                await util.sleep(30);
              }
            }
          }
        }      
      }
      slackService.log('info', 'fin amenity data '+ moment().format() , 'Cron Dynamic Package'); 
    } catch (error) {
      console.log("error",JSON.stringify(error));
      slackService.log('info', 'error amenity data :' + JSON.stringify(error), 'Cron Dynamic Package'); 
    }
}


function parseAmenities(_hotelAmenities,_usedAmenities,_isAllInclusive){
  let parsed = {};
  _.forEach(_hotelAmenities,function(amenity){
    if(_usedAmenities[amenity.AttributeID]){
      if(_isAllInclusive && _usedAmenities[amenity.AttributeID].type === 'BREAKFAST'){
        parsed['ALL-INCLUSIVE'] = {};
        parsed['ALL-INCLUSIVE'].consistency = [true];
        parsed['ALL-INCLUSIVE'].attribute = '666';
      } else {
        if(!parsed[_usedAmenities[amenity.AttributeID].type]){
          parsed[_usedAmenities[amenity.AttributeID].type] = {};
          parsed[_usedAmenities[amenity.AttributeID].type].consistency = [];
        }
        parsed[_usedAmenities[amenity.AttributeID].type].consistency.push((_usedAmenities[amenity.AttributeID].free));
        parsed[_usedAmenities[amenity.AttributeID].type].attribute = amenity.AttributeID;
      }
    }
  });
  return parsed;
}

async function getRegions(_region){
  let _getRegionsUrl = (_region) ? Koa.config.path.getRegions+'/'+_region : Koa.config.path.getRegions;
  return new Promise((resolve, reject) => {
      ws.get('getRegions', _getRegionsUrl , null, null, function(err, result){
        if(err){
          resolve(null);
        } else {
          resolve(result);
        }
      });
  });
}


async function getHotels(_region){
  let _getHotelsUrl = Koa.config.path.getRegionHotels + _region;  
  return new Promise((resolve, reject) => {
      ws.get('getHotelsFromRegion', _getHotelsUrl , null, null, function(err, result){
        if(err){
          resolve(null);
        } else {
          resolve(result);
        }
      });
  });
}

async function insertAmenities(_filename,_amenities){
  var fd = fs.openSync(_filename, 'r');
  var bufferSize = 1024;
  var buffer = new Buffer(bufferSize);

  var leftOver = '';
  var read, line, idxStart, idx;
  while ((read = fs.readSync(fd, buffer, 0, bufferSize, null)) !== 0) {
    leftOver += buffer.toString('utf8', 0, read);
    idxStart = 0
    while ((idx = leftOver.indexOf("\n", idxStart)) !== -1) {
      line = leftOver.substring(idxStart, idx);
      line = parseAmenityLine(line);
      if(line && _amenities[line.AttributeID]){
        await amenityModel.insert(line.EANHotelID,line.AttributeID,line.LanguageCode,line.AppendTxt,'raw');
      }        
      //console.log("one line read: " + line);
      idxStart = idx + 1;
    }
    leftOver = leftOver.substring(idxStart);
  }
  return true;
} 

async function insertHotels(_filename){
  var fd = fs.openSync(_filename, 'r');
  var bufferSize = 1024;
  var buffer = new Buffer(bufferSize);

  var leftOver = '';
  var read, line, idxStart, idx;
  while ((read = fs.readSync(fd, buffer, 0, bufferSize, null)) !== 0) {
    leftOver += buffer.toString('utf8', 0, read);
    idxStart = 0
    while ((idx = leftOver.indexOf("\n", idxStart)) !== -1) {
      line = leftOver.substring(idxStart, idx);
      line = parseHotelLine(line);
      if(line){
        await hotelModel.insert(line.EANHotelID,line.PropertyCategory);
      }        
      //console.log("one line read: " + line);
      idxStart = idx + 1;
    }
    leftOver = leftOver.substring(idxStart);
  }
  return true;
} 

function parseHotelLine(_line){
  let pieces = _line.split('|');
  if(isNaN(pieces[0])){
    return false;
  }
  if(parseFloat(pieces[14]) < 3){
    return false;
  }
  return {
     EANHotelID:(pieces[0]?pieces[0]:'')
    ,PropertyCategory:(pieces[12]?pieces[12]:'')
  }
}

function parseAmenityLine(_line){
  let pieces = _line.split('|');
  if(isNaN(pieces[0])){
    return false;
  }
  return {
     EANHotelID:(pieces[0]?pieces[0]:'')
    ,AttributeID:(pieces[1]?pieces[1]:'')
    ,LanguageCode:(pieces[2]?pieces[2]:'')
    ,AppendTxt:(pieces[3]?pieces[3]:'')
  }
}

async function getAmenityTypes(){
  let _getAmenitiesUrl = Koa.config.path.getAmenityTypes;  
  return new Promise((resolve, reject) => {
      ws.get('getAmenities', _getAmenitiesUrl , null, null, function(err, result){
        if(err){
          resolve(null);
        } else {
          resolve(parseAmenityTypes(result));
        }
      });
  }); 
}

function parseAmenityTypes(_amenityTypes){
  let parsed = {};
  _.forEach(_amenityTypes,function(amenity){
    parsed[amenity.AttributeID] = {
       free: amenity.free.data[0]
      ,type: amenity.type
    };
  })
  return parsed;
}

async function download(url, dest) {
  return new Promise((resolve, reject) => {
      var file = fs.createWriteStream(dest, {'flags': 'w'});
      var request = http.get(url, function(response) {
        response.pipe(file);
        file.on('finish', function() {
          file.close(resolve(true));  // close() is async, call cb after close completes.
        });
      }).on('error', function(err) { // Handle errors
        fs.unlink(dest); // Delete the file async. (But we don't check the result)
        reject(err.message);
      });
  });
};

async function unzipFile(_origin,_destination){
  return new Promise((resolve, reject) => {
    fs.createReadStream(_origin).pipe(unzip.Extract({ path: _destination })).on('close', function() {
        resolve(true); 
    }).on('error', function(err) { // Handle errors
        fs.unlink(dest);
        reject(err.message);
    });  
  });
};

module.exports = {
  feedAmenityData:feedAmenityData
}
