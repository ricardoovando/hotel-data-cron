'use strict';
/* jshint strict: false, esversion: 6 */

function addRooms(_rooms, _params) {
  let numRoom = 1;
  _.each(_rooms, (room, index) => {
		if (/^room\d+$/.test(index) && room) {
      _params['room' + numRoom] = room;
			numRoom++;
		}
  });
  
  return _params;
}

function getProperty(_object, _property) {
	var found;
	for (let i in _object) {
		if (i === _property) {
			found = _object[i];
		} else
    if (typeof _object[i] === 'object') {
    	found = getProperty(_object[i], _property);
    }
    if (found) {
    	return found;
    }
	}
  return null;
}

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms);
    })
}

module.exports = {
  addRooms: addRooms,
  getProperty: getProperty,
  sleep:sleep
};