'use strict';
/* jshint strict: false, esversion: 6 */

const CREDENTIALS = Koa.config.credentials;
const utils = require('./UtilService');
const crypto = require('crypto');
const ws = require('cocha-external-services').webServices;


async function getRequest(_params, _url, _credentials) {
	_params.apiExperience = Koa.config.expediaApiConfig.apiExperience;
	_params.currencyCode = Koa.config.expediaApiConfig.currencyCode;
	_params.locale = Koa.config.expediaApiConfig.locale;
	_params.minorRev = Koa.config.expediaApiConfig.minorRev;
	_params.cid = CREDENTIALS[_credentials.type][_credentials.rate].cid;
	_params.apiKey = CREDENTIALS[_credentials.type][_credentials.rate].keyApi;
	_params.sig = crypto.createHash('md5').update(_params.apiKey + CREDENTIALS[_credentials.type][_credentials.rate].secret + moment().utc().unix()).digest("hex");

	let response;	
	try {
		response = await get(_params, _url);
		if (utils.getProperty(response, 'EanWsError')) {
			throw JSON.stringify(response);
		}
	} catch (error) {
		return false;
	}
	return response;
}

async function get(_params, _uri, _header) {
	let options = {
		method: 'GET',
		uri: _uri,
		qs: _params || null,
		headers: _header || {}
	};

	return new Promise((resolve, reject) => {
	    ws.get('ean', _uri , _params, null, function(err, result){
		    if(err){
		      reject(false);
		    } else {
		      resolve(result);
		    }
	    });
	});


}

module.exports = {
	get:getRequest
};