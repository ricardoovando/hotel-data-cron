'use strict';
/* jshint strict: false, esversion: 6 */

let mysqlService = require('../../config/mysqlDatasource');

async function insert(_hotel,_attribute,_language,_append,_option){
	let query = ' REPLACE INTO amenity_hotel'+
	( _option ? '_raw':'' )+
    ' ('+
    ' EANHotelID,'+
    ' AttributeID,'+
    ' LanguageCode,'+
    ' AppendTxt'+
    ' ) '+
    ' VALUES '+
    ' ( '+
    " '"+ _hotel +"',"+
    " '"+ _attribute +"',"+
    " '"+ _language +"',"+
    " '"+ _append +"'"+
    ' ) ';
	return await mysqlService.query(query,'cocha');
}

async function clear(_option){
	let query = ' DELETE FROM amenity_hotel_raw';
	return await mysqlService.query(query,'cocha');
}

async function del(_hotel,_option){
	let query = ' DELETE FROM amenity_hotel'+
	( _option ? '' : '' )+
	" WHERE EANHotelID = '" + _hotel + "'";
	return await mysqlService.query(query,'cocha');
}

async function getHotelAmenities(_hotel){
	let query = ' SELECT * FROM amenity_hotel_raw '+
	" WHERE EANHotelID = '" + _hotel + "'";
	return await mysqlService.query(query,'cocha');
}

module.exports = {
   insert:insert
  ,clear:clear
  ,del:del
  ,getHotelAmenities:getHotelAmenities
}