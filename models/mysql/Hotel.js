'use strict';
/* jshint strict: false, esversion: 6 */

let mysqlService = require('../../config/mysqlDatasource');

async function insert(_hotel,_propertyCategory){
	let query = ' REPLACE INTO active_property '+
    ' ('+
    ' EANHotelID,'+
    ' PropertyCategory '+
    ' ) '+
    ' VALUES '+
    ' ( '+
    " '"+ _hotel +"',"+
    " '"+ _propertyCategory +"'"+
    ' ) ';
	return await mysqlService.query(query,'cocha');
}

async function isAllInclusive(_hotel){
  let query = ' select EANHotelID,PropertyCategory '+
    ' FROM '+
    ' active_property  '+
    " where EANHotelID = '"+ _hotel +"' "+
    ' ';
  let result = await mysqlService.query(query,'cocha');
  //console.log("RESULT "+_hotel,JSON.stringify(result[0]));
  if(result && result[0] && result[0].PropertyCategory === 8){
    return true;
  } else {
    return false;
  }
}



module.exports = {
    insert:insert
   ,isAllInclusive:isAllInclusive
}